#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <sys/types.h>
#include <time.h>
#include <pthread.h>
#include <signal.h>

pthread_t tid[2];
static volatile int running = 1;

void* socket_thread(void *ptr);
void sig_handler(void);

void* socket_thread(void *ptr) {
	int connfd = 0, n = 0, c = 0;
	struct sockaddr_in client_addr;

    c = sizeof(struct sockaddr_in);

    while(running) {
		connfd = accept((int)ptr, (struct sockaddr*)&client_addr, &c);
		if (connfd >= 0) {
			printf("[Detection] IP address: %s port: %d\n", inet_ntoa(client_addr.sin_addr),(int) ntohs(client_addr.sin_port));
			close(connfd);
		}
		sleep(1);
	}
	pthread_exit(0);
}

void sig_handler() {
	running = 0;
}

int main(int argc, char* argv[]) {
	int i = 0,j = 0;
	int err;
	int ports[2] = {2224, 2225};
	int listenfd[2];
	struct sockaddr_in serv_addr[2];

	printf("Canary - A lightweight solution for the detection of service enumeration attempts\n");
	printf("Starting...\n");

	while (i < 2) {
        	//printf("i: %i\n", i);
        	listenfd[i] = socket(AF_INET, SOCK_STREAM, 0);
        	memset(&serv_addr[i], '0', sizeof(serv_addr[i]));
        	serv_addr[i].sin_family = AF_INET;
        	serv_addr[i].sin_addr.s_addr = htonl(INADDR_ANY);
        	serv_addr[i].sin_port = htons(ports[i]);
        	bind(listenfd[i], (struct sockaddr*)&serv_addr[i], sizeof(serv_addr[i]));
        	listen(listenfd[i], 1);
        	i++;
	}

    signal(SIGINT, sig_handler);

	while (running) {
		while (j < 2) {
            err = pthread_create(&(tid[j]), NULL, &socket_thread, (void *)listenfd[j]);
			if (err != 0)
				printf("\nCan't create thread %s", strerror(err));
			j++;
		}
    }

	printf("\nCancelling threads\n");
	pthread_cancel(tid[0]);
	pthread_cancel(tid[1]);
	return 0;
}
